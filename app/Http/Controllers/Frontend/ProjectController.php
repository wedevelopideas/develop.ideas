<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\developideas\Projects\Repositories\ProjectsRepository;

class ProjectController extends Controller
{
    /**
     * @var ProjectsRepository
     */
    private $projectsRepository;

    /**
     * ProjectController constructor.
     * @param ProjectsRepository $projectsRepository
     */
    public function __construct(ProjectsRepository $projectsRepository)
    {
        $this->projectsRepository = $projectsRepository;
    }

    /**
     * Get project by slug.
     *
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(string $slug)
    {
        $project = $this->projectsRepository->getProjectBySlug($slug);

        return view('project.view')
            ->with('project', $project);
    }
}
