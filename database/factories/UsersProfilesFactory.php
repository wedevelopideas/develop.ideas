<?php

use Faker\Generator as Faker;

$factory->define(\App\developideas\Users\Models\UsersProfiles::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(\App\developideas\Users\Models\Users::class)->create()->id;
        },
        'profile' => $faker->randomElement(['github', 'bitbucket', 'trello']),
        'username' => $faker->userName,
    ];
});
