<?php

namespace App\developideas\Users\Repositories;

use App\developideas\Users\Models\UsersProfiles;

class UsersProfilesRepository
{
    /**
     * @var UsersProfiles
     */
    private $usersProfiles;

    /**
     * UsersProfilesRepository constructor.
     * @param UsersProfiles $usersProfiles
     */
    public function __construct(UsersProfiles $usersProfiles)
    {
        $this->usersProfiles = $usersProfiles;
    }

    /**
     * Get social media entries by user id.
     *
     * @param int $user_id
     * @return mixed
     */
    public function getSocialMediaByUserID(int $user_id)
    {
        return $this->usersProfiles
            ->where('user_id', $user_id)
            ->get();
    }
}
