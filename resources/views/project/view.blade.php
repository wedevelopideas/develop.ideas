@extends('layouts.frontend')
@section('title', $project->name.' | develop.ideas')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="code branch icon"></i>
                        <span class="content">
                            {{ $project->name }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="four wide column">
                    <h4 class="ui header">
                        {{ trans('common.statistics') }}
                    </h4>
                    <div class="ui list">
                        @if ($project->owner())
                        <div class="item">
                            <i class="user icon"></i>
                            <div class="content">
                                <a href="{{ route('user', ['user_name' => $project->owner->user_name]) }}" class="header">{{ $project->owner->display_name }}</a>
                            </div>
                        </div>
                        @endif
                        @if ($project->domain)
                        <div class="item">
                            <i class="external icon"></i>
                            <div class="content">
                                <a href="{{ $project->domain }}" target="_blank" class="header">{{ trans('common.domain') }}</a>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection