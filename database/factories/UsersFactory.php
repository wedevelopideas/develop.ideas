<?php

use Faker\Generator as Faker;

$factory->define(\App\developideas\Users\Models\Users::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('password'),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'display_name' => $faker->name,
        'user_name' => str_slug($faker->name),
        'lang' => $faker->languageCode,
        'remember_token' => str_random(10),
    ];
});
