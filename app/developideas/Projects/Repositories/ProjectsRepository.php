<?php

namespace App\developideas\Projects\Repositories;

use App\developideas\Projects\Models\Projects;

class ProjectsRepository
{
    /**
     * @var Projects
     */
    private $projects;

    /**
     * ProjectsRepository constructor.
     * @param Projects $projects
     */
    public function __construct(Projects $projects)
    {
        $this->projects = $projects;
    }

    /**
     * Get all projects.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->projects
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    /**
     * Get the project by the slug.
     *
     * @param string $slug
     * @return mixed
     */
    public function getProjectBySlug(string $slug)
    {
        return $this->projects
            ->where('slug', $slug)
            ->first();
    }

    /**
     * Get all projects owned by the user.
     *
     * @param int $id
     * @return mixed
     */
    public function getProjectsByUserId(int $id)
    {
        return $this->projects
            ->where('owner_id', $id)
            ->orderBy('name', 'ASC')
            ->get();
    }
}
