<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\developideas\Users\Repositories\UsersRepository;

class HomeController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * HomeController constructor.
     * @param UsersRepository $usersRepository
     */
    public function __construct(UsersRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

    /**
     * Show the backend dashboard.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = $this->usersRepository->count();

        return view('backend.index')
            ->with('users', $users);
    }
}
