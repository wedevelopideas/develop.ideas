<?php

namespace App\developideas\Projects\Models;

use Illuminate\Database\Eloquent\Model;
use App\developideas\Users\Models\Users;

/**
 * Class Projects.
 *
 * @property int $id
 * @property int $owner_id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $domain
 */
class Projects extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'owner_id',
        'name',
        'slug',
        'description',
        'domain',
    ];

    /**
     * Get the owner associated with the project.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function owner()
    {
        return $this->hasOne(Users::class, 'id', 'owner_id');
    }
}
