<?php

namespace Tests\Feature;

use Tests\TestCase;

class ProjectsTest extends TestCase
{
    /** @test */
    public function test_show_the_projects_list()
    {
        $response = $this->actingAs($this->user)->get(route('projects'));
        $response->assertSuccessful();
    }
}
