<?php

use Faker\Generator as Faker;

$factory->define(\App\developideas\Projects\Models\Projects::class, function (Faker $faker) {
    return [
        'owner_id' => function () {
            return factory(\App\developideas\Users\Models\Users::class)->create()->id;
        },
        'name' => $faker->unique()->name,
        'slug' => $faker->slug,
        'description' => $faker->text,
        'domain' => $faker->domainName,
    ];
});
