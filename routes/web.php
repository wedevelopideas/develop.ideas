<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Backend', 'prefix' => 'backend', 'middleware' => 'auth'], function () {
    Route::get('', ['uses' => 'HomeController@index', 'as' => 'backend']);

    Route::group(['prefix' => 'users'], function () {
        Route::get('', ['uses' => 'UsersController@index', 'as' => 'backend.users']);
    });
});

Route::group(['namespace' => 'Frontend'], function () {
    Route::get('', ['uses' => 'HomeController@index', 'as' => 'index']);

    Route::group(['namespace' => 'Auth'], function () {
        Route::get('login', ['uses' => 'LoginController@showLoginForm', 'as' => 'login']);
        Route::post('login', ['uses' => 'LoginController@handleLogin']);
        Route::get('logout', ['uses' => 'LoginController@handleLogout', 'as' => 'logout']);
    });

    Route::group(['middleware' => 'auth'], function () {
        Route::get('dashboard', ['uses' => 'HomeController@dashboard', 'as' => 'dashboard']);

        Route::group(['prefix' => 'user'], function () {
            Route::get('{user_name}', ['uses' => 'UsersController@profile', 'as' => 'user']);
        });

        Route::get('myprojects', ['uses' => 'MyProjectsController@index', 'as' => 'myprojects']);

        Route::group(['prefix' => 'project'], function () {
            Route::get('{slug}', ['uses' => 'ProjectController@view', 'as' => 'project.view']);
        });

        Route::group(['prefix' => 'projects'], function () {
            Route::get('', ['uses' => 'ProjectsController@index', 'as' => 'projects']);
        });
    });
});
