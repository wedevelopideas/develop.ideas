@extends('layouts.frontend')
@section('title', 'develop.ideas')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="four wide column">
                <h4 class="ui header">{{ trans('common.projects') }}</h4>
                <div class="ui divided list">
                    @foreach($projects as $project)
                        <div class="item">
                            <i class="code branch icon"></i>
                            <div class="content">
                                <div class="header">{{ $project->name }}</div>
                            </div>
                        </div>
                    @endforeach
                    <div class="item">
                        <div class="content">
                            <a href="{{ route('myprojects') }}" class="header">{{ trans('common.my_projects') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection