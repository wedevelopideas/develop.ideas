@extends('layouts.backend')
@section('title', 'develop.ideas')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui blue segment">
                        <h2 class="ui header">
                            <i class="line chart icon"></i>
                            <span class="content">
                                {{ trans('common.statistics') }}
                            </span>
                        </h2>
                        <div class="ui statistics">
                            <div class="statistic">
                                <div class="value">
                                    <i class="users icon"></i> {{ $users }}
                                </div>
                                <div class="label">
                                    {{ trans('common.users') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection