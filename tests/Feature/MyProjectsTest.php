<?php

namespace Tests\Feature;

use Tests\TestCase;

class MyProjectsTest extends TestCase
{
    /** @test */
    public function test_shows_all_projects()
    {
        $response = $this->actingAs($this->user)->get(route('myprojects'));
        $response->assertSuccessful();
    }
}
