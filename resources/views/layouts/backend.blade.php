<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
    <title>@yield('title')</title>
    <meta name="description" content="develop.ideas - We develop ideas">
    <link rel="stylesheet" href="{{ asset('assets/semantic.min.css') }}">
    <style type="text/css">
        body {
            background-color: #FFFFFF;
        }

        .main.container {
            margin-top: 7em;
            padding: 0 1em;
        }
        @yield('styles')
    </style>
</head>
<body>
<div class="ui fixed inverted menu">
    <div class="ui fluid container">
        <a href="{{ route('backend') }}" class="header item">
            develop.ideas
        </a>
        <a href="{{ route('backend.users') }}" class="item">
            {{ trans('common.users') }}
        </a>
        <div class="right menu">
            <div class="ui inline dropdown item">
                <div class="text">
                    <i class="dropdown icon"></i>
                    {{ auth()->user()->first_name }}
                </div>
                <div class="menu">
                    <a href="{{ route('dashboard') }}" class="item">
                        <i class="dashboard icon"></i> {{ trans('common.dashboard') }}
                    </a>
                    <div class="ui divider"></div>
                    <a href="{{ route('logout') }}" class="item">
                        <i class="sign out icon"></i> {{ trans('common.logout') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@yield('content')
<script src="{{ asset('assets/jquery.min.js') }}"></script>
<script src="{{ asset('assets/semantic.min.js') }}"></script>
<script>
    $('.ui.dropdown')
        .dropdown()
    ;
</script>
@yield('scripts')
</body>
</html>