<?php

namespace Tests;

use App\developideas\Users\Models\Users;
use App\developideas\Projects\Models\Projects;
use App\developideas\Users\Models\UsersProfiles;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    /**
     * @var Projects
     */
    protected $project;

    /**
     * @var Users
     */
    protected $user;

    /**
     * @var UsersProfiles
     */
    protected $userProfiles;

    /**
     * Set up the test.
     */
    public function setUp()
    {
        parent::setUp();

        $this->project = factory(Projects::class)->create();
        $this->userProfiles = factory(UsersProfiles::class)->create();
        $this->user = factory(Users::class)->create();
    }

    /**
     * Reset the migrations.
     */
    public function tearDown()
    {
        $this->artisan('migrate:reset');
        parent::tearDown();
    }
}
