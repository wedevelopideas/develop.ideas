@extends('layouts.frontend')
@section('title', $user->display_name.' | develop.ideas')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <fieldset class="ui blue segment" style="border-top-right-radius: 25px;border-bottom-right-radius: 25px;border-top: solid 10px #2185d0 !important;border-right: solid 10px #2185d0 !important;border-bottom: solid 10px #2185d0 !important;">
                        <legend style="line-height: 1; text-align: left; padding-right: 20px;margin-right: 20px;">
                            <h2 class="ui header" style="margin: 0;font-size: 25px;line-height: 30px;">{{ $user->display_name }}</h2>
                        </legend>
                    </fieldset>
                </div>
            </div>
            <div class="row">
                <div class="four wide column">
                    @if (count($profiles))
                    <div class="ui blue segment" style="border-top-right-radius: 25px;border-bottom-right-radius: 25px;border-top: solid 10px #2185d0 !important;border-right: solid 10px #2185d0 !important;border-bottom: solid 10px #2185d0 !important;">
                        <div class="ui list">
                        @foreach($profiles as $profile)
                            @if ($profile->profile == 'facebook')
                                <div class="item">
                                    <i class="large blue facebook middle aligned icon"></i>
                                    <div class="content">
                                        <a href="{{ $profile->facebook() }}" target="_blank" class="header">{{ $profile->username }}</a>
                                    </div>
                                </div>
                            @endif
                            @if ($profile->profile == 'twitter')
                                <div class="item">
                                    <i class="large blue twitter middle aligned icon"></i>
                                    <div class="content">
                                        <a href="{{ $profile->twitter() }}" target="_blank" class="header">{{ $profile->username }}</a>
                                    </div>
                                </div>
                            @endif
                            @if ($profile->profile == 'instagram')
                                <div class="item">
                                    <i class="large blue instagram middle aligned icon"></i>
                                    <div class="content">
                                        <a href="{{ $profile->instagram() }}" target="_blank" class="header">{{ $profile->username }}</a>
                                    </div>
                                </div>
                            @endif
                            @if ($profile->profile == 'github')
                                <div class="item">
                                    <i class="large blue github middle aligned icon"></i>
                                    <div class="content">
                                        <a href="{{ $profile->github() }}" target="_blank" class="header">{{ $profile->username }}</a>
                                    </div>
                                </div>
                            @endif
                            @if ($profile->profile == 'bitbucket')
                            <div class="item">
                                <i class="large blue bitbucket middle aligned icon"></i>
                                <div class="content">
                                    <a href="{{ $profile->bitbucket() }}" target="_blank" class="header">{{ $profile->username }}</a>
                                </div>
                            </div>
                            @endif
                            @if ($profile->profile == 'trello')
                            <div class="item">
                                <i class="large blue trello middle aligned icon"></i>
                                <div class="content">
                                    <a href="{{ $profile->trello() }}" target="_blank" class="header">{{ $profile->username }}</a>
                                </div>
                            </div>
                            @endif
                        @endforeach
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection