<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\developideas\Projects\Repositories\ProjectsRepository;

class ProjectsController extends Controller
{
    /**
     * @var ProjectsRepository
     */
    private $projectsRepository;

    /**
     * ProjectsController constructor.
     * @param ProjectsRepository $projectsRepository
     */
    public function __construct(ProjectsRepository $projectsRepository)
    {
        $this->projectsRepository = $projectsRepository;
    }

    /**
     * Show all projects.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $projects = $this->projectsRepository->getAll();

        return view('projects.index')
            ->with('projects', $projects);
    }
}
