<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\developideas\Users\Repositories\UsersRepository;
use App\developideas\Users\Repositories\UsersProfilesRepository;

class UsersController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * @var UsersProfilesRepository
     */
    private $usersProfilesRepository;

    /**
     * UsersController constructor.
     * @param UsersRepository $usersRepository
     * @param UsersProfilesRepository $usersProfilesRepository
     */
    public function __construct(
        UsersRepository $usersRepository,
        UsersProfilesRepository $usersProfilesRepository
    ) {
        $this->usersRepository = $usersRepository;
        $this->usersProfilesRepository = $usersProfilesRepository;
    }

    /**
     * Show the user profile.
     *
     * @param string $user_name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile(string $user_name)
    {
        $user = $this->usersRepository->getUserByUserName($user_name);

        $profiles = $this->usersProfilesRepository->getSocialMediaByUserID($user->id);

        return view('users.profile')
            ->with('profiles', $profiles)
            ->with('user', $user);
    }
}
