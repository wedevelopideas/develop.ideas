<?php

namespace App\developideas\Users\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UsersProfiles.
 *
 * @property int $user_id
 * @property string $profile
 * @property string $username
 */
class UsersProfiles extends Model
{
    /**
     * Part of the Bitbucket url.
     *
     * @var string
     */
    const BITBUCKET = 'https://bitbucket.org/';

    /**
     * Part of the Facebook url.
     */
    const FACEBOOK = 'https://facebook.com/';

    /**
     * Part of the Github url.
     *
     * @var string
     */
    const GITHUB = 'https://github.com/';

    /**
     * Part of the Instagram url.
     *
     * @var string
     */
    const INSTAGRAM = 'https://www.instagram.com/';

    /**
     * Part of the Trello url.
     *
     * @var string
     */
    const TRELLO = 'https://trello.com/';

    /**
     * Part of the Twitter url.
     *
     * @var string
     */
    const TWITTER = 'https://twitter.com/';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'profile',
        'username',
    ];

    /**
     * Bitbucket url.
     *
     * @return string
     */
    public function bitbucket()
    {
        return self::BITBUCKET.$this->username;
    }

    /**
     * Facebook url.
     *
     * @return string
     */
    public function facebook()
    {
        return self::FACEBOOK.$this->username;
    }

    /**
     * Github url.
     *
     * @return string
     */
    public function github()
    {
        return self::GITHUB.$this->username;
    }

    /**
     * Instagram url.
     *
     * @return string
     */
    public function instagram()
    {
        return self::INSTAGRAM.$this->username;
    }

    /**
     * Trello url.
     *
     * @return string
     */
    public function trello()
    {
        return self::TRELLO.$this->username;
    }

    /**
     * Twitter url.
     *
     * @return string
     */
    public function twitter()
    {
        return self::TWITTER.$this->username;
    }
}
