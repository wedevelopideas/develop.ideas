@extends('layouts.frontend')
@section('title', trans('common.my_projects').' | develop.ideas')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="code branch icon"></i>
                        <span class="content">
                            {{ trans('common.my_projects') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <p>You have a total of {{ $projects->count() }} projects, {{ auth()->user()->first_name }}.</p>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui items">
                        @foreach($projects as $project)
                            <div class="item">
                                <div class="content">
                                    <div class="header">
                                        {{ $project->name }}
                                    </div>
                                    <div class="meta">
                                        <i class="lock icon"></i> Closed project
                                        <i class="time icon"></i> {{ $project->created_at->diffForHumans() }}
                                        <i class="user icon"></i> 2 members
                                    </div>
                                    <div class="extra">
                                        Bitucket project
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection