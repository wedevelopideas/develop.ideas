<?php

namespace App\developideas\Users\Repositories;

use App\developideas\Users\Models\Users;

class UsersRepository
{
    /**
     * @var Users
     */
    private $users;

    /**
     * UsersRepository constructor.
     * @param Users $users
     */
    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    /**
     * Number of users.
     *
     * @return mixed
     */
    public function count()
    {
        return $this->users
            ->count();
    }

    /**
     * Get all users.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->users
            ->orderBy('last_name', 'ASC')
            ->orderBy('first_name', 'ASC')
            ->get();
    }

    /**
     * Get the user by username.
     *
     * @param string $user_name
     * @return mixed
     */
    public function getUserByUserName(string $user_name)
    {
        return $this->users
            ->where('user_name', $user_name)
            ->first();
    }
}
