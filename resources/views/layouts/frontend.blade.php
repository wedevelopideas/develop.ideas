<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
    <title>@yield('title')</title>
    <meta name="description" content="develop.ideas - We develop ideas">
    <link rel="stylesheet" href="{{ asset('assets/semantic.min.css') }}">
    <style type="text/css">
        body {
            background-color: #FFFFFF;
        }

        .main.container {
            margin-top: 7em;
        }

        .ui.footer.segment {
            margin: 5em 0 0;
            padding: 5em 0;
        }
        @yield('styles')
    </style>
</head>
<body>
<div class="ui fixed inverted menu">
    <div class="ui container">
        <a href="{{ route('dashboard') }}" class="header item">
            develop.ideas
        </a>
        <a href="{{ route('projects') }}" class="item">
            {{ trans('common.projects') }}
        </a>
        <div class="right menu">
            <div class="ui inline dropdown item">
                <div class="text">
                    <i class="dropdown icon"></i>
                    {{ auth()->user()->first_name }}
                </div>
                <div class="menu">
                    <a href="{{ route('user', ['user_name' => auth()->user()->user_name]) }}" class="item">
                        <i class="user icon"></i> {{ auth()->user()->display_name }}
                    </a>
                    <a href="{{ route('backend') }}" class="item">
                        <i class="dashboard icon"></i> {{ trans('common.administration') }}
                    </a>
                    <a href="{{ route('logout') }}" class="item">
                        <i class="sign out icon"></i> {{ trans('common.logout') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@yield('content')
<div class="ui inverted vertical footer segment">
    <div class="ui center aligned container">
        <div class="ui stackable inverted divided grid">
            <div class="three wide column">
                <h4 class="ui inverted header">Group 1</h4>
                <div class="ui inverted link list">
                    <a href="#" class="item">Link One</a>
                    <a href="#" class="item">Link Two</a>
                    <a href="#" class="item">Link Three</a>
                    <a href="#" class="item">Link Four</a>
                </div>
            </div>
            <div class="three wide column">
                <h4 class="ui inverted header">Group 2</h4>
                <div class="ui inverted link list">
                    <a href="#" class="item">Link One</a>
                    <a href="#" class="item">Link Two</a>
                    <a href="#" class="item">Link Three</a>
                    <a href="#" class="item">Link Four</a>
                </div>
            </div>
            <div class="three wide column">
                <h4 class="ui inverted header">Group 3</h4>
                <div class="ui inverted link list">
                    <a href="#" class="item">Link One</a>
                    <a href="#" class="item">Link Two</a>
                    <a href="#" class="item">Link Three</a>
                    <a href="#" class="item">Link Four</a>
                </div>
            </div>
            <div class="seven wide column">
                <h4 class="ui inverted header">Footer Header</h4>
                <p>Extra space for a call to action inside the footer that could help re-engage users.</p>
            </div>
        </div>
        <div class="ui inverted section divider"></div>
        <div class="ui horizontal inverted small divided link list">
            <a class="item" href="#">Site Map</a>
            <a class="item" href="#">Contact Us</a>
            <a class="item" href="#">Terms and Conditions</a>
            <a class="item" href="#">Privacy Policy</a>
        </div>
    </div>
</div>
<script src="{{ asset('assets/jquery.min.js') }}"></script>
<script src="{{ asset('assets/semantic.min.js') }}"></script>
<script>
    $('.ui.dropdown')
        .dropdown()
    ;
</script>
@yield('scripts')
</body>
</html>