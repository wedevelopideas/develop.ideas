<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\developideas\Projects\Repositories\ProjectsRepository;

class MyProjectsController extends Controller
{
    /**
     * @var ProjectsRepository
     */
    private $projectsRepository;

    /**
     * MyProjectsController constructor.
     * @param ProjectsRepository $projectsRepository
     */
    public function __construct(ProjectsRepository $projectsRepository)
    {
        $this->projectsRepository = $projectsRepository;
    }

    /**
     * Shows the dashboard of all projects associated to the user.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user_id = auth()->user()->id;

        $projects = $this->projectsRepository->getProjectsByUserId($user_id);

        return view('myprojects.index')
            ->with('projects', $projects);
    }
}
