<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\developideas\Projects\Repositories\ProjectsRepository;

class HomeController extends Controller
{
    /**
     * @var ProjectsRepository
     */
    private $projectsRepository;

    /**
     * HomeController constructor.
     * @param ProjectsRepository $projectsRepository
     */
    public function __construct(ProjectsRepository $projectsRepository)
    {
        $this->middleware('auth')->except('index');
        $this->projectsRepository = $projectsRepository;
    }

    /**
     * Show the landing page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('index');
    }

    /**
     * Show the user dashboard.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboard()
    {
        $user_id = auth()->id();

        $projects = $this->projectsRepository->getProjectsByUserId($user_id);

        return view('dashboard')
            ->with('projects', $projects);
    }
}
