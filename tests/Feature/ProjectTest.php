<?php

namespace Tests\Feature;

use Tests\TestCase;

class ProjectTest extends TestCase
{
    /** test */
    public function test_show_the_project_page()
    {
        $response = $this->actingAs($this->user)->get(route('project.view', ['slug' => $this->project->slug]));
        $response->assertSuccessful();
    }
}
