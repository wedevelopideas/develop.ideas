<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class UsersTest extends TestCase
{
    /** @test */
    public function test_show_the_user_list()
    {
        $response = $this->actingAs($this->user)->get(route('backend.users'));
        $response->assertSuccessful();
    }
}
