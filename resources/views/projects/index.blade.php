@extends('layouts.frontend')
@section('title', trans('common.projects').' | develop.ideas')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="code branch icon"></i>
                        <span class="content">
                            {{ trans('common.projects') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui items">
                        @foreach($projects as $project)
                        <div class="item">
                            <div class="content">
                                <a href="{{ route('project.view', ['slug' => $project->slug]) }}" class="header">{{ $project->name }}</a>
                                <div class="meta">
                                    <span>Description</span>
                                </div>
                                <div class="description">
                                    <p></p>
                                </div>
                                <div class="extra">
                                    @if ($project->owner())
                                        {{ $project->owner->display_name }}
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection