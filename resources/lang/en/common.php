<?php

return [
    'actions' => 'Actions',
    'administration' => 'Administration',
    'dashboard' => 'Dashboard',
    'domain' => 'Domain',
    'email' => 'Email',
    'login' => 'Login',
    'logout' => 'Logout',
    'my_projects' => 'My projects',
    'name' => 'Name',
    'password' => 'Password',
    'projects' => 'Projects',
    'statistics' => 'Statistics',
    'users' => 'Users',
];
